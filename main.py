import random, datetime
from flask import Flask, render_template, request, jsonify, url_for
from elsiefour_algorithm import elsie_four as LC4
from weather_checker import get_city, get_weather

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/elsiefour', methods=['GET', 'POST'])
def elsiefour():
    if request.method == 'POST':
        # Find out which form it is based off of the button name
        if request.form['btn'] == 'new-key':
            base = "#_23456789abcdefghijklmnopqrstuvwxyz"
            key = ''.join(random.sample(base, len(base)))
            return jsonify(key=key)
        elif request.form['btn'] == 'encrypt':
            key = request.form['key']
            message = request.form['message']
            ciphertext = LC4(key, message)
            return jsonify(cipher=ciphertext)
        elif request.form['btn'] == 'decrypt':
            key = request.form['key']
            cipher = request.form['message']
            message = LC4(key, '%' + cipher)
            return jsonify(message=message)
    return render_template('elsiefour.html')

@app.route('/weather', methods=['GET', 'POST'])
def weather():
    if request.method == 'POST':
        # Find out which form it is based off of the button name
        if request.form['btn'] == 'search':
            search_string = request.form['search_string']
            city_list = get_city(search_string)
            return jsonify(city_list=city_list)
        elif request.form['btn'] == 'choice':
            identifier = request.form['identifier']
            return url_for('weather_results', identifier=identifier)
    return render_template('weather.html')

@app.route('/weather/<identifier>')
def weather_results(identifier):
    forecast = get_weather(identifier)
    def day_finder(unix_time):
        d = datetime.date.fromtimestamp(unix_time)
        return d.strftime('%A')

    def weather_type(code):
        num = code//100
        if num == 2:
            return "storms.svg"
        elif num == 3:
            return "drizzle.svg"
        elif num == 5:
            return "raining.svg"
        elif num == 6:
            return "snow.svg"
        elif num == 8:
            if code - 800 > 2:
                return "cloudy.svg"
            elif code - 800 > 0:
                return "partlycloudy.svg"
        return "sunny.svg"

    return render_template('forecast.html', forecast=forecast['list'][::8],
                                            city_details=forecast['city'],
                                            day_finder=day_finder,
                                            weather_type=weather_type)

@app.errorhandler(404)
def page_not_found(_):
    return render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=True)
