$(document).ready(function() {
    $('#city_search').on('submit', e => {
        $.ajax({
            data: {
                btn : 'search',
                search_string : $('#search_string').val(),
            },
            type: 'POST',
            url: '/weather',
        })
        .done(function(data) {
            let dropdown = $('#weather_dropdown');
            dropdown.empty();
            $.each(data['city_list'], (_, val) => {
                dropdown.append($('<option></option>').attr('value', val.id).text(val.name + ", " + val.country));
            });
            $('#city_search').hide();
            $('#city_choice').show();
        });
        e.preventDefault()
    });
    $('#city_choice').on('submit', e => {
        $.ajax({
            type: 'POST',
            url: '/weather',
            data: {
                identifier: $("#weather_dropdown option:selected").attr("value"),
                btn : 'choice',
            },
            success: data => {
                window.location = data;
            }
        })
        e.preventDefault()
    });

    var input = document.getElementById("search_string");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("search_btn").click();
    }}); 

    var input = document.getElementById("weather_dropdown");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("select_btn").click();
    }}); 
});