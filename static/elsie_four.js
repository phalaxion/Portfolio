$(document).ready(function() {
    $('#new-key').on('submit', e => {
        $.ajax({
            data: {
                btn : 'new-key'
            },
            type: 'POST',
            url: '/elsiefour',
        })
        .done(function(data) {
            $('#key').text(data.key).show();
        });
        e.preventDefault()
    });

    $('#get-cipher').on('submit', e => {
        $.ajax({
            data: {
                btn : 'encrypt',
                key : $('#encrypt-key').val(),
                message : $('#encrypt-message').val(),
            },
            type: 'POST',
            url: '/elsiefour',
        })
        .done(function(data) {
            $('#cipher').text(data.cipher).show();
            $('#cphrbar').show()
        });
        e.preventDefault()
    });

    $('#get-message').on('submit', e => {
        $.ajax({
            data: {
                btn : 'decrypt',
                key : $('#decrypt-key').val(),
                message : $('#decrypt-message').val(),
            },
            type: 'POST',
            url: '/elsiefour',
        })
        .done(function(data) {
            $('#message').text(data.message).show();
            $('#msgbar').show()
        });
        e.preventDefault()
    });
});